import React from "react"
import SEO from "../components/seo"
import Loadable from "@loadable/component"
import Layout from "../components/layout"
import Container from "@material-ui/core/Container"
import { Typography } from "@material-ui/core"


const LoadableViz = Loadable(() => import("../components/Viz"))

const Viz = () => {
  return (
    <Layout location="visualization">
      <SEO title="Visualization" />
      <Container maxWidth="lg">
        <Typography variant="h3">Mabel Dodge Luhan Network Visualization</Typography>
        <LoadableViz/>      
      </Container> 
    </Layout>
  )
}

export default  Viz