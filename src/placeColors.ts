export default function color(d: string) {
  switch (d) {
    case "Mexico City":
      return "#006847"
    case "Taos, New Mexico":
      return "#FFD700"
    case "New York, New York":
      return "#003884"
    case "Philadelphia":
      return "#C0C0C0"
    default:
      return "#fff"
  }
}