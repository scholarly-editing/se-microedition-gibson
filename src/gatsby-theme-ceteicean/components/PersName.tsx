import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import cloneDeep from "clone-deep"

import { TBehavior } from "gatsby-theme-ceteicean/src/components/DefaultBehaviors"

import { Behavior } from "gatsby-theme-ceteicean/src/components/Behavior"

import { TEINodes } from "react-teirouter"

import { PersonContext } from "./Ceteicean"
import { makeStyles } from "@material-ui/core/styles"

export interface Person {
  name: string
  ref?: string
  places?: string[]
  notes: string
  image?: string
  caption?: string
  bibliography?: string
  descriptors?: {
    type: string
    function: string
    gloss: string
  }[]
}

interface TEIProps {
  teiNode: Node,
  availableRoutes?: string[]
}

const PersName: TBehavior = (props: TEIProps) => {
  const useStyles = makeStyles((theme) => ({
    name: {
      borderBottom: `4px solid ${theme.palette.primary.light}`,
      cursor: "pointer"
    }
  }))
  const classes = useStyles()

  const { setPerson } = React.useContext(PersonContext)
  const peopleData = useStaticQuery(graphql`
    query person {
      allPeopleJson {
        nodes {
          ref
          notes
          name
          image
          caption
          bibliography
          descriptors {
						type
            function
            gloss
          }
        }
      }
    }  
  `)

  const persName = props.teiNode as Element
  const ref = persName.getAttribute('ref')
  let person: Person | null = null
  if (ref) {
    person = peopleData.allPeopleJson.nodes.filter((p: Person) => p.ref === ref.replace('#', ''))[0]
  }
  return (<Behavior node={props.teiNode}>
    <>
      <span className={classes.name}
      onClick={() => {
        // cloneDeep causes the state to refresh even if the same person is clicked.
        setPerson(cloneDeep(person))
      }}
      >{<TEINodes 
          teiNodes={props.teiNode.childNodes}
          {...props}/>}</span>
    </>
  </Behavior>)
}

export default PersName
