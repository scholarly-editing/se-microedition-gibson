import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import { GatsbyImage, IGatsbyImageData } from "gatsby-plugin-image"
import Ceteicean, {Routes} from "gatsby-theme-ceteicean/src/components/Ceteicean"
import {
  Tei,
  TeiHeader
} from "gatsby-theme-ceteicean/src/components/DefaultBehaviors"
import PersName, { Person } from "./PersName"
import Pb from "./Pb"

import { ThemeProvider } from "@material-ui/core/styles"
import CssBaseline from "@material-ui/core/CssBaseline"
import Grid from "@material-ui/core/Grid"
import Container from "@material-ui/core/Container"
import Card from "@material-ui/core/Card"
import CardContent from "@material-ui/core/CardContent"
import CardHeader from "@material-ui/core/CardHeader"
import CardMedia from "@material-ui/core/CardMedia"
import Typography from "@material-ui/core/Typography"
import IconButton from "@material-ui/core/IconButton"
import Close from '@material-ui/icons/Close'
import useMediaQuery from '@material-ui/core/useMediaQuery'

import theme from "../../theme"

import Layout from "../../components/layout"
import { makeStyles } from '@material-ui/core/styles'
import PersonDialog from '../../components/PersonDialog'
import PersonDescriptors from "../../components/PersonDescriptor"
import SEO from "../../components/seo"

interface Props {
  pageContext: {
    name: string
    prefixed: string
    elements: string[]
  },
  location: string
}

export interface Fac {
  name: string
  childImageSharp: {
    gatsbyImageData: IGatsbyImageData
  }
}

type PersonContextType = {
  person: Person | null
  setPerson: React.Dispatch<React.SetStateAction<Person | null>>
}

export const PersonContext = React.createContext<PersonContextType>({
  person: null,
  setPerson: () => console.warn('no person data provider')
})

const MdlCeteicean = ({pageContext}: Props) => {
  const [person, setPerson] = React.useState<Person | null>(null)
  const [cardPosition, setCardPosition] = React.useState(250)

  const cardRef = React.useRef(null)

  const useStyles = makeStyles(() => ({
    person: {
      maxWidth: "300px",
      position: "absolute",
    },
    personImage: {
      textAlign: "center"
    },
    caption: {
      padding: "0 1em"
    },
    biblio: {
      paddingTop: "1em"
    }
  }))
  const classes = useStyles()

  const queryData = useStaticQuery(graphql`
    query general {
      facs: allFile(filter: {relativeDirectory: {in: "facs"}}) {
        nodes {
          name
          childImageSharp {
            gatsbyImageData
          }
        }
      }
      people: allFile(filter: {relativeDirectory: {in: "people"}}) {
        nodes {
          name
          ext
          childImageSharp {
            gatsbyImageData
          }
        }
      }
    }
  `)
  const facs: Fac[] = queryData.facs.nodes

  const routes: Routes = {
    "tei-tei": Tei,
    "tei-teiheader": TeiHeader,
    "tei-persname": PersName,
    "tei-pb": (props) => <Pb facs={facs} {...props}/>,
  }

  React.useEffect(() => {
    const fromTop = document.documentElement.scrollTop > 0 ? document.documentElement.scrollTop : document.body.scrollTop
    setCardPosition(fromTop > 0 ? fromTop : 250)
  }, [person])

  const isScreenSmall = useMediaQuery(theme.breakpoints.down('sm'))

  let personInfo: string
  let notes: JSX.Element | undefined = undefined
  let imgData: IGatsbyImageData | undefined = undefined
  let tags: JSX.Element | undefined = undefined
  if (person) {
    personInfo = person.notes ? person.notes : ""
    const closePerson = (<IconButton aria-label="close person info" onClick={() => setPerson(null)}>
      <Close />
    </IconButton>)

    if (person?.image !== '') {
      imgData = queryData.people.nodes.filter((i: any) => person.image === i.name + i.ext)[0]
        .childImageSharp.gatsbyImageData
    }

    if (person?.descriptors) {
      tags = <PersonDescriptors person={person} key={person.name} />
    }

    if (isScreenSmall) {
      notes = <PersonDialog imgData={imgData} person={person} setPerson={setPerson}/>
    } else {
      const image = imgData ? <GatsbyImage image={imgData} alt={person.name}/> : ''
      notes = ( 
        <Card className={classes.person} {...{ ref: cardRef } as any} style={{top: cardPosition}}>
          <CardHeader
            action={closePerson}
            title={person.name}
          />
          <CardMedia className={classes.personImage}>
            {image}
            <Typography variant="caption" component="div" className={classes.caption}>
              <p dangerouslySetInnerHTML={{
                __html: person.caption || ""
              }}/>
            </Typography>
          </CardMedia>
          <CardContent>
            <Typography variant="body2" color="textSecondary" component="p" dangerouslySetInnerHTML={{
              __html: personInfo
            }}/>
            {tags}
            <Typography variant="body2" color="textSecondary" component="p" className={classes.biblio} dangerouslySetInnerHTML={{
              __html: person.bibliography || ""
            }}/>
          </CardContent>
        </Card>
      )
    }
  }

  let content = (
    <Grid container spacing={1}>
      <Grid item xs={3}/>
      <Grid item xs={6}>
        <Container maxWidth="md" component="main">
          <Ceteicean pageContext={pageContext} routes={routes} />
        </Container>
      </Grid>
      <Grid item xs={3}>
        {notes}
      </Grid>
    </Grid>
  )

  if (isScreenSmall) {
    content = (<Container maxWidth="md" component="main">
    <Ceteicean pageContext={pageContext} routes={routes} />
    {notes}
  </Container>)
  }

  return(
    <PersonContext.Provider value={{person, setPerson}}>
      <Layout location="mdl">
        <SEO title="Edition" />
        <ThemeProvider theme={theme}>
          <CssBaseline />
          {content}
        </ThemeProvider>
      </Layout>
    </PersonContext.Provider>
  )

}

export default MdlCeteicean
