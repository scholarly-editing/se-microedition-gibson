import React from "react"
import { Behavior } from "gatsby-theme-ceteicean/src/components/Behavior"
import { GatsbyImage } from "gatsby-plugin-image"
import { makeStyles } from '@material-ui/core/styles'
import Accordion from '@material-ui/core/Accordion'
import AccordionSummary from '@material-ui/core/AccordionSummary'
import AccordionDetails from '@material-ui/core/AccordionDetails'
import Typography from '@material-ui/core/Typography'
import Paper from '@material-ui/core/Paper'
// import useMediaQuery from '@material-ui/core/useMediaQuery'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'

import {Fac} from "./Ceteicean"
import theme from "../../theme"

interface TEIProps {
  teiNode: Node,
  availableRoutes?: string[]
  facs: Fac[]
}

export type PbBehavior = (props: TEIProps) => JSX.Element | null

const Pb: PbBehavior = ({teiNode, facs}: TEIProps) => {
  const useStyles = makeStyles(() => ({
    root: {
      width: '100%',
      margin: '1em 0',
      transition: 'all .5s ease-in-out',
      gridArea: "1/1",
      position: "relative",
      zIndex: 1,
      backgroundColor: 'transparent',
    },
    expanded: {
      transition: 'all .5s ease-in-out',
      width: '100%',
      margin: '1em 0'
    },
    ease: {
      transition: 'all .5s ease-in-out',
    },
    container: {
      display: 'grid',
    },
    background: {
      width: '100%',
      margin: '1em 0',
      zIndex: -1,
      gridArea: "1/1",
      height: '50.5px',
      opacity: .2,
      overflow: 'clip',
    },
    simple: {
      padding: '1em'
    },
    summary: {
      margin: '8px 0 0 0',
    },
    heading: {
      fontSize: theme.typography.pxToRem(15),
      flexBasis: '33.33%',
      flexShrink: 0,
    },
    secondaryHeading: {      
      fontSize: theme.typography.pxToRem(15),
      color: theme.palette.text.secondary,      
    },
    secondaryHeadingMd: {
      flexBasis: '33.33%',
      flexShrink: 0,
      textAlign: 'center',
    }
  }))
  const classes = useStyles()

  // const [toggleLabel, setToggleLabel] = React.useState('Show')
  const [expanded, setExpanded] = React.useState(false)
  const handleChange = () => {
    // const label = toggleLabel === 'Show' ? 'Hide' : 'Show'
    // setToggleLabel(label)
    setExpanded(!expanded)
  }

  const pb = teiNode as Element
  const n = pb.getAttribute('n') || ''
  const facRef = pb.getAttribute('facs') || ''
  const img = facs.filter(f => f.name === facRef)[0]

  let page: JSX.Element | undefined = undefined

  if (img) {
    // const isScreenLarge = useMediaQuery(theme.breakpoints.up('lg'))
    // const secondaryHeadingStyle = isScreenLarge ? `${classes.secondaryHeading} ${classes.secondaryHeadingMd}` : classes.secondaryHeading
    const background: JSX.Element | undefined = expanded ? undefined : (<GatsbyImage
      className={`${classes.background} ${classes.ease}`}
      image={img.childImageSharp.gatsbyImageData}
      alt=""/> ) 
    page = (
      <div className={classes.container}>
        {background}
        <Accordion classes={{root: classes.root, expanded: classes.expanded}} onChange={() => handleChange()}>
          <AccordionSummary    
            classes={{content: classes.summary}}
            expandIcon={<ExpandMoreIcon />}
            aria-controls="pb-content"
            id="pb-header"
          >
            <Typography className={classes.heading}>{n}</Typography>
            {/* <Typography className={secondaryHeadingStyle}>{toggleLabel} page image</Typography> */}
          </AccordionSummary>
          <AccordionDetails style={{justifyContent: "center"}}>
            <GatsbyImage
              className={classes.ease}
              image={img.childImageSharp.gatsbyImageData}
              alt={`Image of page ${n}`}/>
          </AccordionDetails>
        </Accordion>
      </div>
    )
  } else if (n) {
    page = <Paper className={`${classes.root} ${classes.simple}`} elevation={1}>{n}</Paper>
  }

  if (n) {
    return (
      <Behavior node={teiNode}>        
        {page}
      </Behavior>    
    )
  }
  return null
}

export default Pb
