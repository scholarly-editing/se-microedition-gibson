import React from "react"
import { Box, Dialog, DialogContent, DialogContentText, DialogTitle, IconButton, makeStyles, Slide, Typography } from "@material-ui/core"
import { TransitionProps } from "@material-ui/core/transitions"
import { Close } from "@material-ui/icons"
import { GatsbyImage, IGatsbyImageData } from "gatsby-plugin-image"

import { Person } from '../gatsby-theme-ceteicean/components/PersName'
import PersonDescriptors from "./PersonDescriptor"

interface Props {
  imgData: IGatsbyImageData | undefined
  person: Person
  setPerson: React.Dispatch<React.SetStateAction<Person | null>>
}

const PersonDialog = ({imgData, person, setPerson}: Props) => {

  const useStyles = makeStyles(() => ({
    dialogTitle: {
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center"
    },
    dialogImage: {
      marginBottom: "1rem",
      textAlign: "center"
    }
  }))
  const classes = useStyles()  

  const Transition = React.forwardRef(function Transition(
    props: TransitionProps & { children?: React.ReactElement<any, any> },
    ref: React.Ref<unknown>,
  ) {
    return <Slide direction="up" ref={ref} {...props} />
  })

  let personInfo: string
  let image: JSX.Element | undefined = undefined
  let tags: JSX.Element | undefined = undefined

  personInfo = person.notes ? person.notes : ""

  if (imgData) {
    image = <GatsbyImage image={imgData} alt={person.name || ''}/>
  }

  if (person.descriptors) {
    tags = <PersonDescriptors person={person} />
  }

  return (<Dialog
    open={person.hasOwnProperty('name')}
    scroll="body"
    TransitionComponent={Transition}
    keepMounted
    onClose={() => setPerson(null)}
    aria-labelledby="alert-dialog-slide-title"
    aria-describedby="alert-dialog-slide-description"
  >
    <DialogTitle id="alert-dialog-slide-title" disableTypography className={classes.dialogTitle}>
      <Typography variant="h6">{person.name}</Typography>
      <IconButton aria-label="close person info" onClick={() => setPerson(null)}>
        <Close />
      </IconButton>
    </DialogTitle>
    <DialogContent>
      <Box className={classes.dialogImage}>
        {image}
        <Typography variant="caption" component="div">
          <p dangerouslySetInnerHTML={{
            __html: person.caption || ""
          }}/>
        </Typography>
      </Box>
      <DialogContentText component="div" id="alert-dialog-slide-description">
        <p dangerouslySetInnerHTML={{
          __html: personInfo
        }} />
        {tags}
        <p dangerouslySetInnerHTML={{
          __html: person.bibliography || ""
        }} />
      </DialogContentText>
    </DialogContent>
  </Dialog>)
}

export default PersonDialog
