import React from "react"
import { makeStyles } from "@material-ui/core/styles"
import { Person } from '../gatsby-theme-ceteicean/components/PersName'
import { Chip, Fade, Typography } from "@material-ui/core"

import color from '../placeColors'

interface Props {
  person: Person
}

const PersonDescriptors = ({person}: Props) => {

  const useStyles = makeStyles(() => ({
    chips: {
      display: 'flex',
      justifyContent: 'left',
      flexWrap: 'wrap',
      marginTop: '1rem'
    },
    chip: {
      margin: '4px'
    },
    gloss: {
      border: "1px solid #636363",
      padding: "1ex",
      margin: "1ex",
      backgroundColor: "#ebebeb"
    }
  }))
  const classes = useStyles()

  const [gloss, setGloss] = React.useState<string | undefined>()

  if (person.descriptors) {
    let places: JSX.Element | undefined
    if (person.places) {
      places = (<div className={classes.chips}>{
        person.places.map((d, i) => (
          <Chip key={i} size="small" label={d} className={classes.chip} style={{
            backgroundColor: color(d),
            color: d.startsWith("Taos") ? "#000" : "#fff"
          }} />
        ))
      }</div>)
    }

    return (<>
      {places}

      <Fade in={gloss ? true : false} mountOnEnter unmountOnExit>
        <Typography className={classes.gloss} variant="body2" color="textSecondary" component="p" dangerouslySetInnerHTML={{
          __html: gloss || ""
        }}/>
      </Fade>
      <div className={classes.chips}>{
        person.descriptors.map((d, i) => {
          let color = ''
          switch (d?.function) {
            case "Relational web":
              color = "rgb(119, 170, 221)"
              break
            case "Profession/ Role":
              color = "rgb(68, 187, 153)"
              break
            case "Philosophy":
              color = "rgb(238, 136, 102)"
          }
          return <Chip onClick={() => setGloss(d.gloss)} key={i} size="small" label={d.type} className={classes.chip} style={{
            backgroundColor: color
          }} />
        })
      }</div>
    </>)
  }

  return null

}

export default PersonDescriptors