import React, { useEffect } from "react"
import { graphql, useStaticQuery } from "gatsby"
import { IGatsbyImageData } from "gatsby-plugin-image"
import Chip from "@material-ui/core/Chip"
import LocationOnIcon from '@material-ui/icons/LocationOn'
import { 
  FormControlLabel,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  makeStyles,
  Switch,
  Typography,
  useMediaQuery
} from "@material-ui/core"
import PersonDialog from "../components/PersonDialog"

import * as d3 from "d3"
import { D3DragEvent, Simulation, SimulationLinkDatum, SimulationNodeDatum } from "d3"

import color from '../placeColors'
import theme from "../theme"

// This file is BASED ON code from Mike Bostock and David Bumbeishvili
// released under a GNU General Public License, version 3.

// This file is therefore released under GNU General Public License, version 3,
// in addition to other code base licenses stated in /LICENSE

interface VizData {
  nodes: VizNode[]
  links: QLink[]
}

interface VizNode {
  id: string
  groups: string[]
  image: QImage
}

interface QNode {
  name: string
  places?: string[]
  image?: string
  notes: string
  descriptors?: {
    type: string
    function: string
    gloss: string
  }[]
}

interface QLink {
  source: string
  target: string
  value: number
}

interface QImage {
  name: string
  ext: string
  childImageSharp: {
    thumb: IGatsbyImageData
    gatsbyImageData: IGatsbyImageData
  }
}

interface Tooltip {
  x: number
  y: number
  text: string
}

// Styles

const useStyles = makeStyles(() => ({
  legend: {
    border: "2px solid black",
    padding: "0 20px",
    flex: 1,
    marginBottom: "30px"
  },
  smallFirstPara: {
    display: 'block' 
  },
  label: {
    paddingTop: "15px"
  },
  firstPara: {
    display: "flex",
    gap: "1em",
    "& p": {
      flex: 3
    },
    alignItems: "end"
  }
}))

// Component

const Viz = () => {
  const peopleData = useStaticQuery(graphql`
    query vizd3 {
      allPeopleJson {
        nodes {
          name
          places
          image
          notes
          descriptors {
						type
            function
            gloss
          }
        }
      }
      allLinksJson {
        nodes {
          source,
          target,
          value
        }
      }
      images: allFile(filter: {relativeDirectory: {in: "people"}}) {
        nodes {
          name
          ext
          childImageSharp {
            thumb: gatsbyImageData(width: 80)
            gatsbyImageData
          }
        }
      }
    }  
  `)

  const [multiPlace, setMultiPlace] = React.useState<boolean>(false)

  const data: VizData = {
    nodes: peopleData.allPeopleJson.nodes.reduce((acc: VizNode[], p: QNode) => {
      if (peopleData.allLinksJson.nodes.filter((n: QLink) => (n.source === p.name || n.target === p.name))[0]) {
        const image = peopleData.images.nodes.filter((i: QImage) => p.image === i.name + i.ext)[0]
        const safePlaces = p.places || []
        let places = multiPlace ? safePlaces : [safePlaces[0]]
        // Don't group MLD in default view
        if (!multiPlace && p.name.startsWith('Mabel')) {
          places = []
        }
        acc.push({id: p.name, groups: places as string[], image})
      }
      return acc
    }, []),
    links: peopleData.allLinksJson.nodes
  }

  const placeHolderFace = peopleData.images.nodes.filter((i: QImage) => i.name === 'placeholder')[0]

  const isScreenSmall = useMediaQuery(theme.breakpoints.down('sm'))
  const d3Ref = React.useRef<HTMLDivElement>(null)
  const [tooltip, setTooltip] = React.useState<Tooltip | null>(null)
  const [person, setPerson] = React.useState<QNode | null>(null)

  const width = 900
  const height = 600
  const scaleFactor = multiPlace ? 1.25 : 1.5

  const polygonGenerator = (groupId: string) => {
    const node_coords: [number, number][] = node
      .filter(function(d) { return d.groups.indexOf(groupId) > -1 })
      .data()
      .map(function(d) { return [d.x +25, d.y  +25] })
      
    return d3.polygonHull(node_coords)
  }

  const valueline = d3.line()
    .x((d) => d[0] )
    .y((d) => d[1] )
    .curve(d3.curveCatmullRomClosed)

  function updateGroups(groupIds: string[]) {
    groupIds.forEach(function(groupId: string) {
      let centroid = [0, 0]
      const path = paths.filter((d) => d == groupId)
        .attr('transform', 'scale(1) translate(0,0)')
        .attr('d', (d) => {
          const polygon = polygonGenerator(d) as [number, number][]
          centroid = d3.polygonCentroid(polygon)

          // to scale the shape properly around its points:
          // move the 'g' element to the centroid point, translate
          // all the path around the center of the 'g' and then
          // we can scale the 'g' element properly
          return valueline(
            polygon.map(function(point) {
              return [  point[0] - centroid[0], point[1] - centroid[1] ];
            })
          )
        })

      const pathNode = path.node() as SVGPathElement

      d3.select(pathNode.parentNode as HTMLElement).attr('transform', 'translate('  + centroid[0] + ',' + (centroid[1]) + ') scale(' + scaleFactor + ')')
    })
  }

  const drag = (simulation: Simulation<SimulationNodeDatum, SimulationLinkDatum<SimulationNodeDatum>>) => {
  
    function dragstarted(event: D3DragEvent<SVGImageElement, SimulationNodeDatum, {fx: number, fy: number, x: number, y: number}>) {
      setTooltip(null)
      if (!event.active) simulation.alphaTarget(0.3).restart()
      event.subject.fx = event.subject.x
      event.subject.fy = event.subject.y
    }
    
    function dragged(event: D3DragEvent<SVGImageElement, SimulationNodeDatum, {fx: number, fy: number, x: number, y: number}>) {
      event.subject.fx = event.x;
      event.subject.fy = event.y;
    }
    
    function dragended(event: D3DragEvent<SVGImageElement, SimulationNodeDatum, {fx: number | null, fy: number | null}>) {
      if (!event.active) simulation.alphaTarget(0);
      event.subject.fx = null;
      event.subject.fy = null;
    }
    
    // any: for some reason can't use SVGImageElement even though request type is BaseType | SVGImageElement
    return d3.drag<any, unknown, SVGGElement>() 
        .on("start", dragstarted)
        .on("drag", dragged)
        .on("end", dragended);
  }

  const links = data.links.map(d => Object.create(d))
  const nodes = data.nodes.map(d => Object.create(d))
  const flattened: string[] = []
  const groupIds = flattened.concat.apply([], data.nodes.map(n => n.groups)) // flatten groups
    .filter((v, i, s) => s.indexOf(v) === i) // remove duplicates
    .map(groupId => {
      return {
        groupId,
        count: data.nodes.filter(n => n.groups.indexOf(groupId) > -1).length
      }
    })
    .filter(g => g.count > 2)
    .map(g => g.groupId)

  const groupPositionsX = (d: SimulationNodeDatum, groupIds: string[]) => {
    const person = data.nodes[d.index as number]
    if (person.groups.length === groupIds.length) {
      return width/2
    } else if (person.groups.length === 1 && person.groups[0] === "Mexico City") {
      return width
    } else if (person.groups.length === 1 && person.groups[0] === "New York, New York") {
      return -width
    } else if (person.groups.indexOf("Taos, New Mexico") > -1) {
      return width
    }
    return width
  }

  const groupPositionsY = (d: SimulationNodeDatum, groupIds: string[]) => {
    const person = data.nodes[d.index as number]
    if (person.groups.length === groupIds.length) {
      return height/2
    } else if (person.groups.indexOf("Mexico City") > -1) {
      return -height
    } else if (person.groups.indexOf("New York, New York") > -1) {
      return -height
    } else if (person.groups.indexOf("Taos, New Mexico") > -1) {
      return height/2
    }
    return height
  }

  const simulation = d3.forceSimulation(nodes)
      .force("link", d3.forceLink(links).id(d => {
        // This type casting is awkward and likely should be handled by extending SimulationNodeDatum
        const l = d as VizNode; return l.id
      }).distance((d) => {
        const groupsFactor = data.nodes.filter((n) => n.id === data.links[d.index].target)[0].groups.length + 1
        return 100 / groupsFactor
      }))
      .force("charge", d3.forceManyBody())
      .force("collisionForce", d3.forceCollide(50).strength(1).iterations(1))
      .force("yPosition", d3.forceY((d) => groupPositionsY(d, groupIds)))
      .force("xPosition", d3.forceX((d) => groupPositionsX(d, groupIds)))
      .force("center", d3.forceCenter(width / 2, height / 2).strength(1))

  const svg = d3.create("svg")
      .attr("viewBox", `0 0 ${width} ${height}`)

  const groups = svg.append('g').attr('style', 'fill-opacity: .1; stroke-opacity: 1;')

  const link = svg.append("g")
      .attr("stroke", "#999")
      .attr("stroke-opacity", 0.6)
    .selectAll("line")
    .data(links)
    .join("line")
      .attr("stroke-width", d => Math.sqrt(d.value));

  const node = svg.append("g")
    .selectAll("image")
    .data(nodes)
    .join("image")
      .attr("height", (d) => data.nodes[d.index].id.startsWith('Mabel') ? 80 : 50)
      .attr("style", (d) => {
        let s = "cursor: pointer;"
        if (data.nodes[d.index].id.startsWith('Mabel')) {
          s += "outline: 4px solid #dc3522"
        }
        return s
      })
      .attr("xlink:href", (d) => {
        const image = data.nodes[d.index].image
        if (image) {
          return image?.childImageSharp?.thumb?.images?.fallback?.src
        }
        return placeHolderFace.childImageSharp.thumb.images.fallback.src
      })
      .call(drag(simulation))

  node
  .on('mouseenter', (event, d) => {
    setTooltip({x: event.clientX, y: event.clientY, text: data.nodes[d.index].id})
  })
  .on('mouseleave', () => {
    setTooltip(null)
  })
  .on('click', (_, d) => {
    setPerson(peopleData.allPeopleJson.nodes.filter((p: QNode) => p.name === data.nodes[d.index].id)[0])
  })

  const paths = groups.selectAll('.path_placeholder')
    .data(groupIds)
    .enter()
    .append('g')
    .attr('class', 'path_placeholder')
    .append('path')
    .attr('stroke', (d) => color(d) )
    .attr('fill', (d) => color(d) )
    .attr('opacity', 0)

  paths
    .transition()
    .duration(2000)
    .attr('opacity', 1)

  simulation.on("tick", () => {
    link
        .attr("x1", d => d.source.x +25)
        .attr("y1", d => d.source.y +25)
        .attr("x2", d => d.target.x +25)
        .attr("y2", d => d.target.y +25)

    node
        .attr("x", d => d.x)
        .attr("y", d => d.y)

    updateGroups(groupIds)
  })

  useEffect(() => {
    const el = d3Ref.current
    if (el) {
      while(el.firstChild){
        el.removeChild(el.firstChild)
      }
      d3Ref.current.append(svg.node() as Node)
    }
  
  }, [multiPlace])


  const tooltipEl = tooltip ? <Chip 
    style={{position:"fixed", top: tooltip.y, left: tooltip.x}}
    label={tooltip.text} /> : ''

  let personDialog: JSX.Element | undefined = undefined

  if (person) {
    const image = peopleData.images.nodes.filter((i: QImage) => person.image === i.name + i.ext)[0]
    const imgData: IGatsbyImageData | undefined = image ? image.childImageSharp.gatsbyImageData : undefined
    personDialog = <PersonDialog person={person} imgData={imgData} setPerson={setPerson}/>
  }

  const classes = useStyles()

  return (
    <div>
      {personDialog}
      {tooltipEl}
      <div ref={d3Ref}/>
      <div className={isScreenSmall ? classes.smallFirstPara : classes.firstPara}>        
        <div className={classes.legend}>
          <List dense >
              {groupIds.map((g, i) => {
                return (
                  <ListItem key={i}>
                    <ListItemIcon>
                      <LocationOnIcon style={{fill: color(g)}}/>
                    </ListItemIcon>
                    <ListItemText primary={g} />
                  </ListItem>
                )}
              )}
            </List>
            <FormControlLabel
              control={
                <Switch
                  checked={!multiPlace}
                  onChange={() => setMultiPlace(!multiPlace)}
                  name="checkedB"
                  color="primary"
                />
              }
              label={<Typography className={classes.label} variant="body2">Only group by primary place</Typography>}
            />
        </div>
        <Typography variant="body1" paragraph>
          In “Whirling around Mexico,” Mabel Dodge Luhan describes the many people she meets.
          Sometimes the text of the memoir makes clear how people and ideas connect, but at other
          times, those relationships are not entirely clear from the text. The hyperlinked biographies do
          provide some of that detail, but this graph illustrates the connections with more force.
        </Typography>  
      </div>
      <Typography variant="body1" paragraph>
        The colored shapes represent the primary locations of Mabel Dodge’s circle, thus highlighting
        the transnational nature of her network. During the period the memoir describes the faces on
        the blue background were centered in New York City (with the exception of Leopold Stokowski,
        who was nearby in Philadelphia). Those surrounded by green lived and worked in Mexico City.
        The smallest shape is yellow, and the people here were primarily located in Taos, New Mexico.
        Most of the figures in the graph traveled a great deal, and their locations were unstable. Carlos
        Chávez, Moisés Sáenz, Frank Tannenbaum, Frances Toor, Manuel Gamio, Elsie Clews Parsons,
        and Frances Flynn Paine all conducted careers that depended on frequent travel between the
        U.S. and Mexico.
      </Typography>
      <Typography variant="body1" paragraph>
        The lines show the social connections among those in the circle; in the memoir Mabel Dodge
        writes about where and how they intersect with her, but she does not always elaborate on how
        they connect to one another. For example, it is interesting to observe how many of the actors in
        “Whirling around Mexico” had a link to Columbia University and the philosophies of Franz
        Boas—Manuel Gamio, Frances Toor, Elsie Clews Parsons, Frank Tannenbaum, Frances Flynn
        Paine, and Moisés Sáenz. Therefore, even though Boas does not ever appear in the memoir,
        observing those many connections suggests the pervasiveness of his influence, and allows
        readers to revisit the memoir with that in mind.
      </Typography>
      <Typography variant="body1" paragraph>
        The inclusion of Franz Boas and Manuel Gamio is also a reminder of Mabel Dodge’s reach,
        which extended far beyond the circle described in her prose. Each of the people mentioned
        existed in their own social networks. Although there is not space to illustrate these intersecting
        networks, we can imagine the ideas and inspirations gathered at Mabel Dodge’s events
        spiralling outward into the cultural conversation. We get a taste for how that happens through her
        description of Chávez’s conducting. Mabel Dodge was one of many visitors to Mexico who
        observed the quality of the Orquesta Sinfónica de México, urging friends to patronize their
        concerts. Chávez himself cultivated a U.S. audience to such great success that during the
        summer seasons critics estimated that one-third of the audience was from the US rather than
        Mexico.
      </Typography>
      <Typography variant="body1" paragraph>
        The hyperlinked biographies attached to each photograph are the same ones that appear in the
        memoir text. Nonetheless, by reading them in context with the illustration, one gets a better
        sense for how the actors related to one another, as well as how and where they brought ideas into
        Mabel Dodge’s network from elsewhere and carried thoughts and observations out into their

        own lives. For example, in the section of the memoir where Tony Luhan and Concha Michel are
        participating in a musical competition, Mabel Dodge makes clear the attendees' various
        epiphanies about the value of Native music. Such moments can seem very isolated until one
        considers the reach of their collective networks, which extend beyond national borders. This
        graph sets the stage for that type of mental visualization.
      </Typography>
    </div>
  )
}

export default Viz