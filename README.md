# Mabel Dodge Luhan's “Whirling around Mexico”: A Selection
## Edited by Christina Taylor Gibson, Towson University
## Digital Edition code by Raffaele Viglianti and Christina Taylor Gibson

### Scholarly Editing, Volume 39, 2022

This micro-edition is published on the [Scholarly Editing website](https://scholarlyediting.org/issues/39/mabel-dodge-luhans-whirling-around-mexico-a-selection).

The repository is **archived**. No further changes will be made to this repository. For a general micro-edition template please see [this GitLab repository](https://gitlab.com/scholarly-editing/se-microedition-template).
