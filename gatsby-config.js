const basePath = process.env.BASEPATH
const title = "Mabel Dodge Luhan's “Whirling around Mexico”: A Selection"

module.exports = {
  pathPrefix: basePath,
  siteMetadata: {
    issue: "Volume 39",
    doi: '10.55520/A1P85HWW',
    group_order: 3,
    title: title,
    htmlTitle: title,
    description: `A Scholarly Editing micro-edition. ${title}. Edited by Christina T. Gibson.`,
    authors: [
      {
        "first": "Christina",
        "middle": "Taylor",
        "last": "Gibson",
        "affiliations": [
          "Towson University"
        ],
        orcid:"0000-0003-3010-4532"
      }
    ],
    menuLinks: [
      {
        name: 'introduction',
        link: '/'
      },
      {
        name: 'visualization',
        link: '/visualization'
      },
      {
        name: 'edition',
        link: '/mdl'
      },
    ]
  },
  plugins: [
    `gatsby-plugin-material-ui`,
    `gatsby-theme-ceteicean`,
    `gatsby-transformer-json`,
    `gatsby-plugin-image`,
    `gatsby-plugin-sharp`,
    `gatsby-transformer-sharp`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `static/data`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/src/images/`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `introduction`,
        path: `src/introduction`,
      },
    },
    `gatsby-transformer-remark`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Scholarly Editing`,
        short_name: `Scholarly Editing`,
        start_url: `/`,
        icon: `src/images/se-icon.png`, // This path is relative to the root of the site.
      },
    },
  ],
}
