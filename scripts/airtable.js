#!/usr/bin/env node

require("dotenv").config()
const fs = require('fs')
const path = require('path')
const request = require('request')
const Airtable = require('airtable')

const DATALOCATION = path.resolve(__dirname, '../static/data/')
const IMAGESLOCATION = path.resolve(__dirname, '../src/images/people/')

const airtableKey = process.env.AIRTABLE_API_KEY
if (! airtableKey) {
  console.error('Please add AIRTABLE_API_KEY to your environment or .env file!')
  process.exit()
}
const at = new Airtable({apiKey: airtableKey})

function writeJson(o, filename) {
  const fullPath = path.resolve(DATALOCATION, filename)
  fs.writeFileSync(fullPath, JSON.stringify(o, null, 2) + '\n')
  console.log(`wrote ${fullPath}`)
}

const baseId = process.env.AIRTABLE_ID
if (! baseId) {
  console.error('Please add AIRTABLE_ID to your environment or .env file!')
  process.exit()
}
const base = at.base(baseId)

function getTable(base, table) {
  return new Promise((resolve, reject) => {
    const things = {}
    base(table).select()
      .eachPage(
        async (records, nextPage) => {
          for (const r of records) {
            things[r.id] = r
          }
          nextPage()
        },
        (error) => {
          if (error) {
            console.log(`error while fetching from ${table}: ${error}`)
            reject(error)
          } else {
            resolve(things)
          }
        }
      )
  })
}

function downloadImage(uri, filename){
  request.head(uri, function(err, res, body){
    if (err) {
      console.log(err)
    }
    request(uri).pipe(fs.createWriteStream(path.resolve(IMAGESLOCATION, filename)))
  })
}

async function getData() {
  const peopleTable = await getTable(base, "People")
  const descs = await getTable(base, "Person Descriptors")
  const placesTable = await getTable(base, "Places")
  const people = []
  let peopleOdd = ''
  for (const id in peopleTable) {
    const record = peopleTable[id]
    const pdescs = record.get("Person descriptors") || []
    const descriptors = pdescs.map(d => {
      return {
        type: descs[d].get("types"),
        function: descs[d].get("Function"),
        gloss: descs[d].get("Glossary")
      }
    })
    descriptors.sort((a, b) => (a.function > b.function) ? 1 : -1)
    const placeIds = record.get("Place") || []
    const places = placeIds.reduce((acc, id) => {
      const placeRec = placesTable[id]
      if (placeRec) {
        acc.push(placeRec.get("Name"))
      }
      return acc
    }, [])
    const ref = record.get("ref")
    const name = record.get("Name")
    const photograph = record.get("Photograph")
    const caption = record.get("Caption") || ""
    const bibliography = record.get("Bibliography") || ""
    let image = ""
    if (photograph) {
      const photo = photograph[0]
      image = photo.filename

      // Download image
      // await downloadImage(photo.url, photo.filename)
    }
    if (ref) {
      people.push({
        name,
        notes: record.get("Notes"),
        ref,
        places,
        descriptors,
        image,
        caption,
        bibliography
      })
      peopleOdd += `<valItem mode="add" ident="#${ref}"><desc xml:lang="en">${name}</desc></valItem>`
    } else {
      console.info(`Person ${name} does not have a ref and it's been skipped.`)
    }
    
  }
  console.log(`Copy list of people to ODD:`)
  console.log(`
  <elementSpec ident="persName" mode="change"><attList><attDef ident="ref" mode="change"><valList>
  ${peopleOdd}
  </valList></attDef></attList></elementSpec>
  `)
  writeJson(people, 'people.json')

  // LINKS

  const linksTable = await getTable(base, "Links")
  const links = []

  for (const id in linksTable) {
    const record = linksTable[id]
    const sourceId = record.get("Source")
    const source = peopleTable[sourceId].get("Name")
    const targetId = record.get("Target")
    const target = peopleTable[targetId].get("Name")

    links.push({
      source,
      target,
      value: 1
    })
  }

  writeJson(links, 'links.json')
}

getData()
